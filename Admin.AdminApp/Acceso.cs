﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin.AdminApp
{
    public partial class FrmAcceso : Form
    {
        public FrmAcceso()
        {
            InitializeComponent();
            Bitmap img = new Bitmap(Application.StartupPath+ @"/Img/acceso 3.jpg");
            this.BackgroundImage = img;
        }

        private void Crud_Load(object sender, EventArgs e)
        {

        }

        private void LbTitulo_Click(object sender, EventArgs e)
        {

        }

        private void BtmRegistro_Click(object sender, EventArgs e)
        {
            Form Register = new FrmPersona();
            Register.Show();
            Form Access = new FrmAcceso();
            Access.Close();
        }

        private void btmEntrar_Click(object sender, EventArgs e)
        {
                
        }

        private void LbOlvidecontraseña_Click(object sender, EventArgs e)
        {
            Form recuperar = new FrmPassword();
            recuperar.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Form Register = new FrmPersona();
            Register.Show();
            Form Acceso = new FrmAcceso();
            Acceso.Close();
        }

        private void toolStripTextBox3_Click(object sender, EventArgs e)
        {
            Form Persona = new FrmPersona();
            Persona.Show();
            Form Acceso = new FrmAcceso();
            Acceso.Close();
        }

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripTextBox1_Click(object sender, EventArgs e)
        {
            Form Acceso = new FrmAcceso();
            Acceso.Close();
        }

        private void toolStripTextBox4_Click(object sender, EventArgs e)
        {
            Form Grupo = new FrmGrupo();
            Grupo.Show();
            Form Acceso = new FrmAcceso();
            Acceso.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}

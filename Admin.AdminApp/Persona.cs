﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin.AdminApp
{
    public partial class FrmPersona : Form
    {
        public FrmPersona()
        {
            InitializeComponent();
        }

        private void BtmDelete_Click(object sender, EventArgs e)
        {
            txtClave.Text = "";
            txtUsuario.Text = "";
            txtconfirmar.Text = "";   
        }
        private void BtmRegresar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Form Acceso = new FrmAcceso();
            Acceso.Show();
            Form Persona = new FrmPersona();
            Persona.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            txtClave.Text = "";
            txtUsuario.Text = "";
            txtconfirmar.Text = "";
        }
    }
}

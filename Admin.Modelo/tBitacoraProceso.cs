namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tBitacoraProceso")]
    public partial class tBitacoraProceso
    {
        [Key]
        public int IdBitacoraProceso { get; set; }

        public int IdApp { get; set; }

        public int IdUsuario { get; set; }

        public DateTime FechaRegistro { get; set; }

        public string Descripcion { get; set; }

        public string Diagnostico { get; set; }

        public string Observacion { get; set; }

        public virtual tAplicacion tAplicacion { get; set; }

        public virtual tUsuario tUsuario { get; set; }
    }
}

namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tAplicacion")]
    public partial class tAplicacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tAplicacion()
        {
            tBitacoraAcceso = new HashSet<tBitacoraAcceso>();
            tBitacoraProceso = new HashSet<tBitacoraProceso>();
            tMenu = new HashSet<tMenu>();
            tParametro = new HashSet<tParametro>();
            tPerfil = new HashSet<tPerfil>();
            tPermiso = new HashSet<tPermiso>();
            tReporte = new HashSet<tReporte>();
        }

        [Key]
        public int IdApp { get; set; }

        public int IdTipoAplicacion { get; set; }

        [StringLength(25)]
        public string ClaveDesarrollo { get; set; }

        [Required]
        [StringLength(8)]
        public string ClaveApp { get; set; }

        [Required]
        [StringLength(250)]
        public string App { get; set; }

        [Required]
        [StringLength(30)]
        public string AbrevApp { get; set; }

        [Required]
        [StringLength(250)]
        public string UrlRaizApp { get; set; }

        [StringLength(250)]
        public string URLSoporte { get; set; }

        [StringLength(80)]
        public string EmailSoporte { get; set; }

        [StringLength(20)]
        public string PassEmailSoporte { get; set; }

        [StringLength(50)]
        public string EmailApp { get; set; }

        [StringLength(20)]
        public string PassEmailApp { get; set; }

        [Required]
        [StringLength(300)]
        public string PathLogo { get; set; }

        [Required]
        [StringLength(50)]
        public string Version { get; set; }

        public DateTime FechaVersion { get; set; }

        public string DescripcionVersion { get; set; }

        public bool Activo { get; set; }

        public virtual tTipoAplicacion tTipoAplicacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tBitacoraAcceso> tBitacoraAcceso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tBitacoraProceso> tBitacoraProceso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tMenu> tMenu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tParametro> tParametro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPerfil> tPerfil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPermiso> tPermiso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tReporte> tReporte { get; set; }
    }
}

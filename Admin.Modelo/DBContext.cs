namespace Admin.Modelo
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBContext : DbContext
    {
        public DBContext()
            : base("name=DBContext")
        {
        }

        public virtual DbSet<tAccesibilidad> tAccesibilidad { get; set; }
        public virtual DbSet<tAplicacion> tAplicacion { get; set; }
        public virtual DbSet<tBitacoraAcceso> tBitacoraAcceso { get; set; }
        public virtual DbSet<tBitacoraProceso> tBitacoraProceso { get; set; }
        public virtual DbSet<tGrupo> tGrupo { get; set; }
        public virtual DbSet<tMenu> tMenu { get; set; }
        public virtual DbSet<tParametro> tParametro { get; set; }
        public virtual DbSet<tPerfil> tPerfil { get; set; }
        public virtual DbSet<tPerfilMenu> tPerfilMenu { get; set; }
        public virtual DbSet<tPermiso> tPermiso { get; set; }
        public virtual DbSet<tPersona> tPersona { get; set; }
        public virtual DbSet<tReporte> tReporte { get; set; }
        public virtual DbSet<tReportePerfil> tReportePerfil { get; set; }
        public virtual DbSet<tTipoAplicacion> tTipoAplicacion { get; set; }
        public virtual DbSet<tUsuario> tUsuario { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.ClaveDesarrollo)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.ClaveApp)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.App)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.AbrevApp)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.UrlRaizApp)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.URLSoporte)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.EmailSoporte)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.PassEmailSoporte)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.EmailApp)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.PassEmailApp)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.PathLogo)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.Version)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .Property(e => e.DescripcionVersion)
                .IsUnicode(false);

            modelBuilder.Entity<tAplicacion>()
                .HasMany(e => e.tBitacoraAcceso)
                .WithRequired(e => e.tAplicacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tAplicacion>()
                .HasMany(e => e.tBitacoraProceso)
                .WithRequired(e => e.tAplicacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tAplicacion>()
                .HasMany(e => e.tPermiso)
                .WithRequired(e => e.tAplicacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tBitacoraAcceso>()
                .Property(e => e.IP)
                .IsUnicode(false);

            modelBuilder.Entity<tBitacoraAcceso>()
                .Property(e => e.NombreEquipo)
                .IsUnicode(false);

            modelBuilder.Entity<tBitacoraProceso>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<tBitacoraProceso>()
                .Property(e => e.Diagnostico)
                .IsUnicode(false);

            modelBuilder.Entity<tBitacoraProceso>()
                .Property(e => e.Observacion)
                .IsUnicode(false);

            modelBuilder.Entity<tGrupo>()
                .HasMany(e => e.tParametro)
                .WithRequired(e => e.tGrupo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tMenu>()
                .Property(e => e.Menu)
                .IsUnicode(false);

            modelBuilder.Entity<tMenu>()
                .Property(e => e.Path)
                .IsUnicode(false);

            modelBuilder.Entity<tMenu>()
                .Property(e => e.UrlIcono)
                .IsUnicode(false);

            modelBuilder.Entity<tMenu>()
                .Property(e => e.CssIcono)
                .IsUnicode(false);

            modelBuilder.Entity<tMenu>()
                .Property(e => e.HtmlIcono)
                .IsUnicode(false);

            modelBuilder.Entity<tMenu>()
                .HasMany(e => e.tPerfilMenu)
                .WithRequired(e => e.tMenu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tParametro>()
                .Property(e => e.Parametro)
                .IsUnicode(false);

            modelBuilder.Entity<tParametro>()
                .Property(e => e.Valor)
                .IsUnicode(false);

            modelBuilder.Entity<tParametro>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<tPerfil>()
                .HasMany(e => e.tPerfilMenu)
                .WithRequired(e => e.tPerfil)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tPerfil>()
                .HasMany(e => e.tPermiso)
                .WithRequired(e => e.tPerfil)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tPerfil>()
                .HasMany(e => e.tReportePerfil)
                .WithRequired(e => e.tPerfil)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.ApPaterno)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.ApMaterno)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.NombreCompleto)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.Nacionalidad)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.CodigPostal)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.CURP)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.RFC)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.IdInterno)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<tPersona>()
                .HasMany(e => e.tUsuario)
                .WithRequired(e => e.tPersona)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tReporte>()
                .Property(e => e.NombreReporte)
                .IsUnicode(false);

            modelBuilder.Entity<tReporte>()
                .HasMany(e => e.tReportePerfil)
                .WithRequired(e => e.tReporte)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tTipoAplicacion>()
                .Property(e => e.Clave)
                .IsUnicode(false);

            modelBuilder.Entity<tTipoAplicacion>()
                .Property(e => e.TipoAplicacion)
                .IsUnicode(false);

            modelBuilder.Entity<tTipoAplicacion>()
                .HasMany(e => e.tAplicacion)
                .WithRequired(e => e.tTipoAplicacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tUsuario>()
                .Property(e => e.Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<tUsuario>()
                .Property(e => e.CorreoUsuario)
                .IsUnicode(false);

            modelBuilder.Entity<tUsuario>()
                .HasMany(e => e.tBitacoraAcceso)
                .WithRequired(e => e.tUsuario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tUsuario>()
                .HasMany(e => e.tBitacoraProceso)
                .WithRequired(e => e.tUsuario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tUsuario>()
                .HasMany(e => e.tPermiso)
                .WithRequired(e => e.tUsuario)
                .WillCascadeOnDelete(false);
        }
    }
}

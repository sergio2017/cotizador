namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tAccesibilidad")]
    public partial class tAccesibilidad
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tAccesibilidad()
        {
            tPermiso = new HashSet<tPermiso>();
        }

        [Key]
        public int IdAccesibilidad { get; set; }

        [Required]
        [StringLength(250)]
        public string Protecccion { get; set; }

        [Required]
        [StringLength(100)]
        public string Referencia { get; set; }

        public bool Registrar { get; set; }

        public bool Actualizar { get; set; }

        public bool Eliminar { get; set; }

        public bool Consular { get; set; }

        public bool Activo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPermiso> tPermiso { get; set; }
    }
}

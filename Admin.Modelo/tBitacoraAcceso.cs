namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tBitacoraAcceso")]
    public partial class tBitacoraAcceso
    {
        [Key]
        public int IdBitacoraAcceso { get; set; }

        public int IdApp { get; set; }

        public int IdUsuario { get; set; }

        public DateTime FechaRegistro { get; set; }

        public string IP { get; set; }

        public string NombreEquipo { get; set; }

        public bool? Activo { get; set; }

        public virtual tAplicacion tAplicacion { get; set; }

        public virtual tUsuario tUsuario { get; set; }
    }
}

namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tMenu")]
    public partial class tMenu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tMenu()
        {
            tPerfilMenu = new HashSet<tPerfilMenu>();
        }

        [Key]
        public int IdMenu { get; set; }

        public int? IdApp { get; set; }

        [Required]
        [StringLength(70)]
        public string Menu { get; set; }

        [StringLength(150)]
        public string Path { get; set; }

        [StringLength(300)]
        public string UrlIcono { get; set; }

        [StringLength(70)]
        public string CssIcono { get; set; }

        [StringLength(70)]
        public string HtmlIcono { get; set; }

        public int? MenuPadre { get; set; }

        public int Orden { get; set; }

        public int Nivel { get; set; }

        public bool ShowNameMenu { get; set; }

        public bool Activo { get; set; }

        public virtual tAplicacion tAplicacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPerfilMenu> tPerfilMenu { get; set; }
    }
}

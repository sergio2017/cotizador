namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tReporte")]
    public partial class tReporte
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tReporte()
        {
            tReportePerfil = new HashSet<tReportePerfil>();
        }

        [Key]
        public int IdReporte { get; set; }

        public int? IdApp { get; set; }

        [Required]
        [StringLength(250)]
        public string NombreReporte { get; set; }

        public DateTime FechaReg { get; set; }

        public bool Status { get; set; }

        public virtual tAplicacion tAplicacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tReportePerfil> tReportePerfil { get; set; }
    }
}

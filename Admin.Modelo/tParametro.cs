namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tParametro")]
    public partial class tParametro
    {
        [Key]
        public int IdParametro { get; set; }

        public int? IdApp { get; set; }

        public int IdGrupo { get; set; }

        [Required]
        [StringLength(30)]
        public string Parametro { get; set; }

        [Required]
        [StringLength(300)]
        public string Valor { get; set; }

        [StringLength(250)]
        public string Descripcion { get; set; }

        public bool Heredar { get; set; }

        public bool Activo { get; set; }

        public virtual tAplicacion tAplicacion { get; set; }

        public virtual tGrupo tGrupo { get; set; }
    }
}

namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tTipoAplicacion")]
    public partial class tTipoAplicacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tTipoAplicacion()
        {
            tAplicacion = new HashSet<tAplicacion>();
        }

        [Key]
        public int IdTipoAplicacion { get; set; }

        [Required]
        [StringLength(8)]
        public string Clave { get; set; }

        [Required]
        [StringLength(50)]
        public string TipoAplicacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tAplicacion> tAplicacion { get; set; }
    }
}

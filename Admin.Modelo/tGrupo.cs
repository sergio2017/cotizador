namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tGrupo")]
    public partial class tGrupo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tGrupo()
        {
            tParametro = new HashSet<tParametro>();
        }

        [Key]
        public int IdGrupo { get; set; }

        [Required]
        [StringLength(8)]
        public string AbrevGrupo { get; set; }

        [Required]
        [StringLength(250)]
        public string Grupo { get; set; }

        public bool EsProductivo { get; set; }

        public bool Activo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tParametro> tParametro { get; set; }
    }
}

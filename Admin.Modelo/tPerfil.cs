namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tPerfil")]
    public partial class tPerfil
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tPerfil()
        {
            tPerfilMenu = new HashSet<tPerfilMenu>();
            tPermiso = new HashSet<tPermiso>();
            tReportePerfil = new HashSet<tReportePerfil>();
        }

        [Key]
        public int IdPerfil { get; set; }

        public int? IdApp { get; set; }

        [Required]
        [StringLength(250)]
        public string Perfil { get; set; }

        [StringLength(250)]
        public string Logo { get; set; }

        public bool Heredado { get; set; }

        public bool Activo { get; set; }

        public virtual tAplicacion tAplicacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPerfilMenu> tPerfilMenu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPermiso> tPermiso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tReportePerfil> tReportePerfil { get; set; }
    }
}

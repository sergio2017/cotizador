namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tPermiso")]
    public partial class tPermiso
    {
        [Key]
        public int IdPermiso { get; set; }

        public int IdApp { get; set; }

        public int IdPerfil { get; set; }

        public int IdUsuario { get; set; }

        public int? IdAccesibilidad { get; set; }

        public bool activo { get; set; }

        public virtual tAccesibilidad tAccesibilidad { get; set; }

        public virtual tAplicacion tAplicacion { get; set; }

        public virtual tPerfil tPerfil { get; set; }

        public virtual tUsuario tUsuario { get; set; }
    }
}

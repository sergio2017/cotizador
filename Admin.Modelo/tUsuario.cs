namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tUsuario")]
    public partial class tUsuario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tUsuario()
        {
            tBitacoraAcceso = new HashSet<tBitacoraAcceso>();
            tBitacoraProceso = new HashSet<tBitacoraProceso>();
            tPermiso = new HashSet<tPermiso>();
        }

        [Key]
        public int IdUsuario { get; set; }

        public int IdPersona { get; set; }

        [Required]
        [StringLength(20)]
        public string Usuario { get; set; }

        [Required]
        [StringLength(250)]
        public string Clave { get; set; }

        public int IntentosFallidos { get; set; }

        public bool Bloqueado { get; set; }

        public DateTime FechaRegistro { get; set; }

        [Column(TypeName = "date")]
        public DateTime Vigencia { get; set; }

        public bool? Suspension { get; set; }

        public int? DiasSuspension { get; set; }

        [StringLength(250)]
        public string CorreoUsuario { get; set; }

        [StringLength(250)]
        public string TokenTemp { get; set; }

        [Column(TypeName = "date")]
        public DateTime? VigenciaTemp { get; set; }

        public bool Activo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tBitacoraAcceso> tBitacoraAcceso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tBitacoraProceso> tBitacoraProceso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPermiso> tPermiso { get; set; }

        public virtual tPersona tPersona { get; set; }
    }
}

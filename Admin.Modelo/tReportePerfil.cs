namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tReportePerfil")]
    public partial class tReportePerfil
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdReporte { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdPerfil { get; set; }

        public bool Status { get; set; }

        public virtual tPerfil tPerfil { get; set; }

        public virtual tReporte tReporte { get; set; }
    }
}

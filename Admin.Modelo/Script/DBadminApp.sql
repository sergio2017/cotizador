USE [DBadminApp]
GO
/****** Object:  Table [dbo].[tAccesibilidad]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tAccesibilidad](
	[IdAccesibilidad] [int] IDENTITY(1,1) NOT NULL,
	[Protecccion] [nvarchar](250) NOT NULL,
	[Referencia] [nvarchar](100) NOT NULL,
	[Registrar] [bit] NOT NULL,
	[Actualizar] [bit] NOT NULL,
	[Eliminar] [bit] NOT NULL,
	[Consular] [bit] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_Proteccion] PRIMARY KEY CLUSTERED 
(
	[IdAccesibilidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tAplicacion]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tAplicacion](
	[IdApp] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoAplicacion] [int] NOT NULL,
	[ClaveDesarrollo] [varchar](25) NULL,
	[ClaveApp] [varchar](8) NOT NULL,
	[App] [varchar](250) NOT NULL,
	[AbrevApp] [varchar](30) NOT NULL,
	[UrlRaizApp] [varchar](250) NOT NULL,
	[URLSoporte] [varchar](250) NULL,
	[EmailSoporte] [varchar](80) NULL,
	[PassEmailSoporte] [varchar](20) NULL,
	[EmailApp] [varchar](50) NULL,
	[PassEmailApp] [varchar](20) NULL,
	[PathLogo] [varchar](300) NOT NULL,
	[Version] [varchar](50) NOT NULL,
	[FechaVersion] [datetime] NOT NULL,
	[DescripcionVersion] [varchar](max) NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_tblAplicacion] PRIMARY KEY CLUSTERED 
(
	[IdApp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tBitacoraAcceso]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tBitacoraAcceso](
	[IdBitacoraAcceso] [int] IDENTITY(1,1) NOT NULL,
	[IdApp] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[FechaRegistro] [datetime] NOT NULL,
	[IP] [varchar](max) NULL,
	[NombreEquipo] [varchar](max) NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_tBitacoraAcceso] PRIMARY KEY CLUSTERED 
(
	[IdBitacoraAcceso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tBitacoraProceso]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tBitacoraProceso](
	[IdBitacoraProceso] [int] IDENTITY(1,1) NOT NULL,
	[IdApp] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[FechaRegistro] [datetime] NOT NULL,
	[Descripcion] [varchar](max) NULL,
	[Diagnostico] [varchar](max) NULL,
	[Observacion] [varchar](max) NULL,
 CONSTRAINT [PK_tBitacoraProceso] PRIMARY KEY CLUSTERED 
(
	[IdBitacoraProceso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tGrupo]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tGrupo](
	[IdGrupo] [int] IDENTITY(1,1) NOT NULL,
	[AbrevGrupo] [nvarchar](8) NOT NULL,
	[Grupo] [nvarchar](250) NOT NULL,
	[EsProductivo] [bit] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_Grupo] PRIMARY KEY CLUSTERED 
(
	[IdGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tMenu]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tMenu](
	[IdMenu] [int] IDENTITY(1,1) NOT NULL,
	[IdApp] [int] NULL,
	[Menu] [varchar](70) NOT NULL,
	[Path] [varchar](150) NULL,
	[UrlIcono] [varchar](300) NULL,
	[CssIcono] [varchar](70) NULL,
	[HtmlIcono] [varchar](70) NULL,
	[MenuPadre] [int] NULL,
	[Orden] [int] NOT NULL,
	[Nivel] [int] NOT NULL,
	[ShowNameMenu] [bit] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[IdMenu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tParametro]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tParametro](
	[IdParametro] [int] IDENTITY(1,1) NOT NULL,
	[IdApp] [int] NULL,
	[IdGrupo] [int] NOT NULL,
	[Parametro] [varchar](30) NOT NULL,
	[Valor] [varchar](300) NOT NULL,
	[Descripcion] [varchar](250) NULL,
	[Heredar] [bit] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_Parametro] PRIMARY KEY CLUSTERED 
(
	[IdParametro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tPerfil]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPerfil](
	[IdPerfil] [int] IDENTITY(1,1) NOT NULL,
	[IdApp] [int] NULL,
	[Perfil] [nvarchar](250) NOT NULL,
	[Logo] [nvarchar](250) NULL,
	[Heredado] [bit] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_Perfil] PRIMARY KEY CLUSTERED 
(
	[IdPerfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tPerfilMenu]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPerfilMenu](
	[IdMenu] [int] NOT NULL,
	[IdPerfil] [int] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_tPerfilMenu] PRIMARY KEY CLUSTERED 
(
	[IdMenu] ASC,
	[IdPerfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tPermiso]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPermiso](
	[IdPermiso] [int] IDENTITY(1,1) NOT NULL,
	[IdApp] [int] NOT NULL,
	[IdPerfil] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[IdAccesibilidad] [int] NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_UsuarioPerfil] PRIMARY KEY CLUSTERED 
(
	[IdPermiso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tPersona]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tPersona](
	[IdPersona] [int] IDENTITY(1,1) NOT NULL,
	[FechaRegistro] [datetime] NOT NULL,
	[EsPersonaMoral] [bit] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[ApPaterno] [varchar](50) NULL,
	[ApMaterno] [varchar](50) NULL,
	[NombreCompleto] [varchar](250) NOT NULL,
	[FechaNacimiento] [date] NOT NULL,
	[Nacionalidad] [varchar](50) NOT NULL,
	[Direccion] [varchar](250) NULL,
	[CodigPostal] [varchar](10) NULL,
	[CURP] [varchar](18) NULL,
	[RFC] [varchar](18) NULL,
	[Sexo] [bit] NOT NULL,
	[FechaIngreso] [date] NULL,
	[IdInterno] [varchar](10) NULL,
	[Telefono] [varchar](12) NULL,
	[Email] [varchar](80) NULL,
	[ActiveDirectory] [bit] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_tPersona] PRIMARY KEY CLUSTERED 
(
	[IdPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tReporte]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tReporte](
	[IdReporte] [int] IDENTITY(1,1) NOT NULL,
	[IdApp] [int] NULL,
	[NombreReporte] [varchar](250) NOT NULL,
	[FechaReg] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_tReporte] PRIMARY KEY CLUSTERED 
(
	[IdReporte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tReportePerfil]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tReportePerfil](
	[IdReporte] [int] NOT NULL,
	[IdPerfil] [int] NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_tReportePerfil] PRIMARY KEY CLUSTERED 
(
	[IdReporte] ASC,
	[IdPerfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tTipoAplicacion]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tTipoAplicacion](
	[IdTipoAplicacion] [int] IDENTITY(1,1) NOT NULL,
	[Clave] [varchar](8) NOT NULL,
	[TipoAplicacion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tTipoAplicacion] PRIMARY KEY CLUSTERED 
(
	[IdTipoAplicacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tUsuario]    Script Date: 24/09/2018 03:25:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tUsuario](
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[IdPersona] [int] NOT NULL,
	[Usuario] [varchar](20) NOT NULL,
	[Clave] [nvarchar](250) NOT NULL,
	[IntentosFallidos] [int] NOT NULL,
	[Bloqueado] [bit] NOT NULL,
	[FechaRegistro] [datetime] NOT NULL,
	[Vigencia] [date] NOT NULL,
	[Suspension] [bit] NULL,
	[DiasSuspension] [int] NULL,
	[CorreoUsuario] [varchar](250) NULL,
	[TokenTemp] [nvarchar](250) NULL,
	[VigenciaTemp] [date] NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tGrupo] ADD  CONSTRAINT [DF_tGrupo_EsProductivo]  DEFAULT ((0)) FOR [EsProductivo]
GO
ALTER TABLE [dbo].[tMenu] ADD  CONSTRAINT [DF_tMenu_ShowNameMenu]  DEFAULT ((1)) FOR [ShowNameMenu]
GO
ALTER TABLE [dbo].[tPersona] ADD  CONSTRAINT [DF_tPersona_FechaRegistro]  DEFAULT (getdate()) FOR [FechaRegistro]
GO
ALTER TABLE [dbo].[tPersona] ADD  CONSTRAINT [DF_tPersona_EsPersonaMoral]  DEFAULT ((0)) FOR [EsPersonaMoral]
GO
ALTER TABLE [dbo].[tPersona] ADD  CONSTRAINT [DF_tPersona_Activdierctory]  DEFAULT ((0)) FOR [ActiveDirectory]
GO
ALTER TABLE [dbo].[tAplicacion]  WITH CHECK ADD  CONSTRAINT [FK_tAplicacion_tTipoAplicacion] FOREIGN KEY([IdTipoAplicacion])
REFERENCES [dbo].[tTipoAplicacion] ([IdTipoAplicacion])
GO
ALTER TABLE [dbo].[tAplicacion] CHECK CONSTRAINT [FK_tAplicacion_tTipoAplicacion]
GO
ALTER TABLE [dbo].[tBitacoraAcceso]  WITH CHECK ADD  CONSTRAINT [FK_tBitacoraAcceso_tAplicacion] FOREIGN KEY([IdApp])
REFERENCES [dbo].[tAplicacion] ([IdApp])
GO
ALTER TABLE [dbo].[tBitacoraAcceso] CHECK CONSTRAINT [FK_tBitacoraAcceso_tAplicacion]
GO
ALTER TABLE [dbo].[tBitacoraAcceso]  WITH CHECK ADD  CONSTRAINT [FK_tBitacoraAcceso_tUsuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[tUsuario] ([IdUsuario])
GO
ALTER TABLE [dbo].[tBitacoraAcceso] CHECK CONSTRAINT [FK_tBitacoraAcceso_tUsuario]
GO
ALTER TABLE [dbo].[tBitacoraProceso]  WITH CHECK ADD  CONSTRAINT [FK_tBitacoraProceso_tAplicacion] FOREIGN KEY([IdApp])
REFERENCES [dbo].[tAplicacion] ([IdApp])
GO
ALTER TABLE [dbo].[tBitacoraProceso] CHECK CONSTRAINT [FK_tBitacoraProceso_tAplicacion]
GO
ALTER TABLE [dbo].[tBitacoraProceso]  WITH CHECK ADD  CONSTRAINT [FK_tBitacoraProceso_tUsuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[tUsuario] ([IdUsuario])
GO
ALTER TABLE [dbo].[tBitacoraProceso] CHECK CONSTRAINT [FK_tBitacoraProceso_tUsuario]
GO
ALTER TABLE [dbo].[tMenu]  WITH CHECK ADD  CONSTRAINT [FK_tMenu_tAplicacion] FOREIGN KEY([IdApp])
REFERENCES [dbo].[tAplicacion] ([IdApp])
GO
ALTER TABLE [dbo].[tMenu] CHECK CONSTRAINT [FK_tMenu_tAplicacion]
GO
ALTER TABLE [dbo].[tParametro]  WITH CHECK ADD  CONSTRAINT [FK_Parametro_Grupo] FOREIGN KEY([IdGrupo])
REFERENCES [dbo].[tGrupo] ([IdGrupo])
GO
ALTER TABLE [dbo].[tParametro] CHECK CONSTRAINT [FK_Parametro_Grupo]
GO
ALTER TABLE [dbo].[tParametro]  WITH CHECK ADD  CONSTRAINT [FK_tParametro_tAplicacion] FOREIGN KEY([IdApp])
REFERENCES [dbo].[tAplicacion] ([IdApp])
GO
ALTER TABLE [dbo].[tParametro] CHECK CONSTRAINT [FK_tParametro_tAplicacion]
GO
ALTER TABLE [dbo].[tPerfil]  WITH CHECK ADD  CONSTRAINT [FK_tPerfil_tAplicacion] FOREIGN KEY([IdApp])
REFERENCES [dbo].[tAplicacion] ([IdApp])
GO
ALTER TABLE [dbo].[tPerfil] CHECK CONSTRAINT [FK_tPerfil_tAplicacion]
GO
ALTER TABLE [dbo].[tPerfilMenu]  WITH CHECK ADD  CONSTRAINT [FK_PerfilMenu_Menu] FOREIGN KEY([IdMenu])
REFERENCES [dbo].[tMenu] ([IdMenu])
GO
ALTER TABLE [dbo].[tPerfilMenu] CHECK CONSTRAINT [FK_PerfilMenu_Menu]
GO
ALTER TABLE [dbo].[tPerfilMenu]  WITH CHECK ADD  CONSTRAINT [FK_PerfilMenu_Perfil] FOREIGN KEY([IdPerfil])
REFERENCES [dbo].[tPerfil] ([IdPerfil])
GO
ALTER TABLE [dbo].[tPerfilMenu] CHECK CONSTRAINT [FK_PerfilMenu_Perfil]
GO
ALTER TABLE [dbo].[tPermiso]  WITH CHECK ADD  CONSTRAINT [FK_tPermiso_tAccesibilidad] FOREIGN KEY([IdAccesibilidad])
REFERENCES [dbo].[tAccesibilidad] ([IdAccesibilidad])
GO
ALTER TABLE [dbo].[tPermiso] CHECK CONSTRAINT [FK_tPermiso_tAccesibilidad]
GO
ALTER TABLE [dbo].[tPermiso]  WITH CHECK ADD  CONSTRAINT [FK_tPermiso_tAplicacion] FOREIGN KEY([IdApp])
REFERENCES [dbo].[tAplicacion] ([IdApp])
GO
ALTER TABLE [dbo].[tPermiso] CHECK CONSTRAINT [FK_tPermiso_tAplicacion]
GO
ALTER TABLE [dbo].[tPermiso]  WITH CHECK ADD  CONSTRAINT [FK_tPermiso_tPerfil] FOREIGN KEY([IdPerfil])
REFERENCES [dbo].[tPerfil] ([IdPerfil])
GO
ALTER TABLE [dbo].[tPermiso] CHECK CONSTRAINT [FK_tPermiso_tPerfil]
GO
ALTER TABLE [dbo].[tPermiso]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioPerfil_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[tUsuario] ([IdUsuario])
GO
ALTER TABLE [dbo].[tPermiso] CHECK CONSTRAINT [FK_UsuarioPerfil_Usuario]
GO
ALTER TABLE [dbo].[tReporte]  WITH CHECK ADD  CONSTRAINT [FK_tReporte_tAplicacion] FOREIGN KEY([IdApp])
REFERENCES [dbo].[tAplicacion] ([IdApp])
GO
ALTER TABLE [dbo].[tReporte] CHECK CONSTRAINT [FK_tReporte_tAplicacion]
GO
ALTER TABLE [dbo].[tReportePerfil]  WITH CHECK ADD  CONSTRAINT [FK_tReportePerfil_tPerfil] FOREIGN KEY([IdPerfil])
REFERENCES [dbo].[tPerfil] ([IdPerfil])
GO
ALTER TABLE [dbo].[tReportePerfil] CHECK CONSTRAINT [FK_tReportePerfil_tPerfil]
GO
ALTER TABLE [dbo].[tReportePerfil]  WITH CHECK ADD  CONSTRAINT [FK_tReportePerfil_tReporte] FOREIGN KEY([IdReporte])
REFERENCES [dbo].[tReporte] ([IdReporte])
GO
ALTER TABLE [dbo].[tReportePerfil] CHECK CONSTRAINT [FK_tReportePerfil_tReporte]
GO
ALTER TABLE [dbo].[tUsuario]  WITH CHECK ADD  CONSTRAINT [FK_tUsuario_tPersona] FOREIGN KEY([IdPersona])
REFERENCES [dbo].[tPersona] ([IdPersona])
GO
ALTER TABLE [dbo].[tUsuario] CHECK CONSTRAINT [FK_tUsuario_tPersona]
GO

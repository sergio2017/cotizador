namespace Admin.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tPersona")]
    public partial class tPersona
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tPersona()
        {
            tUsuario = new HashSet<tUsuario>();
        }

        [Key]
        public int IdPersona { get; set; }

        public DateTime FechaRegistro { get; set; }

        public bool EsPersonaMoral { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(50)]
        public string ApPaterno { get; set; }

        [StringLength(50)]
        public string ApMaterno { get; set; }

        [Required]
        [StringLength(250)]
        public string NombreCompleto { get; set; }

        [Column(TypeName = "date")]
        public DateTime FechaNacimiento { get; set; }

        [Required]
        [StringLength(50)]
        public string Nacionalidad { get; set; }

        [StringLength(250)]
        public string Direccion { get; set; }

        [StringLength(10)]
        public string CodigPostal { get; set; }

        [StringLength(18)]
        public string CURP { get; set; }

        [StringLength(18)]
        public string RFC { get; set; }

        public bool Sexo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaIngreso { get; set; }

        [StringLength(10)]
        public string IdInterno { get; set; }

        [StringLength(12)]
        public string Telefono { get; set; }

        [StringLength(80)]
        public string Email { get; set; }

        public bool ActiveDirectory { get; set; }

        public bool Activo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tUsuario> tUsuario { get; set; }
    }
}
